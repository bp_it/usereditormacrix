﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using UserEditor.Entities;
using UserEditor.Repositories.Interfaces;

namespace UserEditor.Repositories
{
    public class XmlFileUsersDataAccessObject : IUsersDataAccessObject
    {
        private const string _filename = "users.xml";

        public IEnumerable<UserEntity> Get()
        {
            var xmlUrl = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), _filename);

            if (!File.Exists(xmlUrl))
            {
                return new List<UserEntity>();
            }

            var serializer = new XmlSerializer(typeof(UserEntities));

            using (var reader = new FileStream(xmlUrl, FileMode.Open))
            {
                return ((UserEntities)serializer.Deserialize(reader)).Users;
            }
        }

        public void Save(IEnumerable<UserEntity> users)
        {
            var serializer = new XmlSerializer(typeof(UserEntities));
            var subReq = new UserEntities();
            subReq.Users = users.ToList();

            var xmlUrl = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), _filename);

            using (var writer = new FileStream(xmlUrl, FileMode.OpenOrCreate))
            {
                writer.SetLength(0);
                serializer.Serialize(writer, subReq);
            }
        }

        private string GetUserLocalFolderPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        }
    }
}
