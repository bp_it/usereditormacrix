﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using UserEditor.Entities;
using UserEditor.Mappers;
using UserEditor.Mappers.Interfaces;
using UserEditor.Models;
using UserEditor.Repositories.Interfaces;

namespace UserEditor.Repositories
{
    public class XmlFileUsersRepository : IUsersRepository
    {
        private readonly IModelMapper<IEnumerable<UserEntity>, IEnumerable<UserModel>> _entitiesToModels = new UserEntitiesToUserModels();
        private readonly IModelMapper<IEnumerable<UserModel>, IEnumerable<UserEntity>> _modelsToEntities = new UserModelsToUserEntities();
        private readonly IUsersDataAccessObject _dao = new XmlFileUsersDataAccessObject();

        public void Save(IEnumerable<UserModel> users)
        {
            var entities = _modelsToEntities.Map(users);

            _dao.Save(entities);
        }

        IEnumerable<UserModel> IUsersRepository.Get()
        {
            var entities = _dao.Get();

            return _entitiesToModels.Map(entities);
        }
    }
}
