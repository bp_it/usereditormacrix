﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEditor.Entities;
using UserEditor.Models;

namespace UserEditor.Repositories.Interfaces
{
    public interface IUsersRepository
    {
        IEnumerable<UserModel> Get();
        void Save(IEnumerable<UserModel> users);
    }
}
