﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEditor.Entities;

namespace UserEditor.Repositories.Interfaces
{
    public interface IUsersDataAccessObject
    {
        IEnumerable<UserEntity> Get();
        void Save(IEnumerable<UserEntity> users);
    }
}
