﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using UserEditor.Models;
using UserEditor.Repositories;
using UserEditor.Repositories.Interfaces;
using UserEditor.WpfScaffold;

namespace UserEditor.ViewModels
{
    public class UserTableEditorViewModel : INotifyPropertyChanged
    {
        #region Commands and Events
        public event PropertyChangedEventHandler PropertyChanged;

        private ICommand _saveClick;
        public ICommand SaveClick { get => _saveClick; set => _saveClick = value; }

        private ICommand _cancelClick;
        public ICommand CancelClick { get => _cancelClick; set => _cancelClick = value; }

        private ICommand _deleteClick;
        public ICommand DeleteClick { get => _deleteClick; set => _deleteClick = value; }
        
        private ICommand _cellEditEndingCommand;
        public ICommand CellEditEndingCommand
        {
            get
            {
                return _cellEditEndingCommand ?? (_cellEditEndingCommand = new RelayCommand(x =>
                {
                    DataChangeDetected(x);
                }));
            }
        }
        
        private ICommand _addingNewItemCommand;
        public ICommand AddingNewItemCommand
        {
            get
            {
                return _addingNewItemCommand ?? (_addingNewItemCommand = new RelayCommand(x =>
                {
                    DataChangeDetected(x);
                }));
            }
        }

        private ICommand _loadedCommand;
        public ICommand LoadedCommand
        {
            get
            {
                return _loadedCommand ?? (_loadedCommand = new RelayCommand(x =>
                {
                    GetUserData();
                }));
            }
        }
        #endregion

        #region Fields and Properties
        private readonly IUsersRepository _usersRepository;

        private ObservableCollection<UserModel> _users;
        public ObservableCollection<UserModel> Users
        {
            get => _users;
            set
            {
                _users = value;
                RaisePropertyChanged(nameof(_users));
            }
        }

        private bool _shouldSave;
        public bool ShouldSave
        {
            get => _shouldSave;
            set
            {
                _shouldSave = value;
                RaisePropertyChanged(nameof(ShouldSave));
            }
        }

        private UserModel _selectedUser;
        public UserModel SelectedUser
        {
            get => _selectedUser;
            set
            {
                if (value != null)
                {
                    _selectedUser = value;
                    RaisePropertyChanged(nameof(SelectedUser));
                }
            }
        }
        #endregion

        #region .ctor
        public UserTableEditorViewModel()
        {
            _usersRepository = new XmlFileUsersRepository();

            ShouldSave = false;
            SaveClick = new RelayCommand(new Action<object>(SaveAction));
            DeleteClick = new RelayCommand(new Action<object>(DeleteAction));
            CancelClick = new RelayCommand(new Action<object>(CancelAction));
            _users = new ObservableCollection<UserModel>();
        }
        #endregion

        #region Methods
        private void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void SaveAction(object sender)
        {
            _usersRepository.Save(_users);
            MarkAllUsersAsSaved();
            ShouldSave = false;
        }

        private void MarkAllUsersAsSaved()
        {
            _users.ToList().ForEach(u => u.IsNew = false);
        }

        public void DeleteAction(object sender)
        {
            if (sender is UserModel)
            {
                Users.Remove((UserModel)sender);
                ShouldSave = true;
            }
        }

        public void CancelAction(object sender)
        {
            GetUserData();

            ShouldSave = false;
        }

        private void GetUserData()
        {
            var newUsers = _usersRepository.Get();
            _users.Clear();

            foreach (var user in newUsers)
            {
                _users.Add(user);
            }
        }

        public void DataChangeDetected(object parameter)
        {
            ShouldSave = true;
        }
        #endregion
    }
}
