﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEditor.Entities;
using UserEditor.Mappers.Interfaces;
using UserEditor.Models;

namespace UserEditor.Mappers
{
    public class UserEntityToUserModel : IModelMapper<UserEntity, UserModel>
    {
        public UserModel Map(UserEntity from)
        {
            return new UserModel
            {
                Age = from.Age,
                ApartmentNumber = from.ApartmentNumber,
                DayOfBirth = from.DayOfBirth,
                FirstName = from.FirstName,
                HouseNumber = from.HouseNumber,
                LastName = from.LastName,
                PhoneNumber = from.PhoneNumber,
                PostalCode = from.PostalCode,
                StreetName = from.StreetName,
                IsNew = false
            };
        }
    }
}
