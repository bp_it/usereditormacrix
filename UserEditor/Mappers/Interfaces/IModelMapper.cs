﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserEditor.Mappers.Interfaces
{
    public interface IModelMapper<TFrom, TTo>
    {
        TTo Map(TFrom from);
    }
}
