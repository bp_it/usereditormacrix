﻿using System.Collections.Generic;
using UserEditor.Entities;
using UserEditor.Mappers.Interfaces;
using UserEditor.Models;

namespace UserEditor.Mappers
{
    public class UserModelsToUserEntities : IModelMapper<IEnumerable<UserModel>, IEnumerable<UserEntity>>
    {
        private readonly IModelMapper<UserModel, UserEntity> _mapper = new UserModelToUserEntity();

        public IEnumerable<UserEntity> Map(IEnumerable<UserModel> from)
        {
            var users = new List<UserEntity>();

            foreach (var userFrom in from)
            {
                users.Add(_mapper.Map(userFrom));
            }

            return users;
        }
    }
}
