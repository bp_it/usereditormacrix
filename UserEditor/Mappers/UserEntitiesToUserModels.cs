﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserEditor.Entities;
using UserEditor.Mappers.Interfaces;
using UserEditor.Models;

namespace UserEditor.Mappers
{
    public class UserEntitiesToUserModels : IModelMapper<IEnumerable<UserEntity>, IEnumerable<UserModel>>
    {
        private readonly IModelMapper<UserEntity, UserModel> _mapper = new UserEntityToUserModel();

        public IEnumerable<UserModel> Map(IEnumerable<UserEntity> from)
        {
            var users = new List<UserModel>();

            foreach (var userFrom in from)
            {
                users.Add(_mapper.Map(userFrom));
            }

            return users;
        }
    }
}
